const reducer = (state = [], action) => {
  switch (action.type) {
    case 'ADD_TODOS':
      return [...state,{
        topic: action.topic,
        completed: false
      }]
    case 'TOGGLE_TODOS':
      return state.map((num, index) => {
        if (index === action.targetIndex){
          return {
            ...num,
            completed: !num.completed
          }
        }
        return num
      })
    case 'REMOVE_TODOS':
      return state.filter((num, index) => {
        return index !== action.targetIndex
      })
    default:
      return state
  }
}


const item={topic:'',completed: false }


let state = []

state = reducer(state, {
  type: 'ADD_TODOS',
  topic: 'เอาผ้าไปซัก'
})

console.log(state)
// Result
// [
//     {
//         topic: 'เอาผ้าไปซัก',
//         completed: false
//     }
// ]

state = reducer(state, {
  type: 'ADD_TODOS',
  topic: 'รดน้ำต้นไม้'
})

console.log(state)

// Result
// [
//     {
//         topic: 'เอาผ้าไปซัก',
//         completed: false
//     },
//     {
//         topic: 'รดน้ำต้นไม้',
//         completed: false
//     }
// ]

state = reducer(state, {
  type: 'ADD_TODOS',
  topic: 'ซื้อพิซซ่า'
})

console.log(state)

// Result
// [
//     {
//         topic: 'เอาผ้าไปซัก',
//         completed: false
//     },
//     {
//         topic: 'รดน้ำต้นไม้',
//         completed: false
//     }
//     {
//         topic: 'ซื้อพิซซ่า',
//         completed: false
//     }
// ]

state = reducer(state, {
  type: 'TOGGLE_TODOS',
  targetIndex: 1
})

console.log(state)


// Result
// [
//     {
//         topic: 'เอาผ้าไปซัก',
//         completed: false
//     },
//     {
//         topic: 'รดน้ำต้นไม้',
//         completed: true
//     }
//     {
//         topic: 'ซื้อพิซซ่า',
//         completed: false
//     }
// ]

state = reducer(state, {
  type: 'TOGGLE_TODOS',
  targetIndex: 1
})

console.log(state)


// Result
// [
//     {
//         topic: 'เอาผ้าไปซัก',
//         completed: false
//     },
//     {
//         topic: 'รดน้ำต้นไม้',
//         completed: false
//     }
//     {
//         topic: 'ซื้อพิซซ่า',
//         completed: false
//     }
// ]

state = reducer(state, {
  type: 'REMOVE_TODOS',
  targetIndex: 1
})

console.log(state)


// Result
// [
//     {
//         topic: 'เอาผ้าไปซัก',
//         completed: false
//     },
//     {
//         topic: 'ซื้อพิซซ่า',
//         completed: false
//     }
// ]