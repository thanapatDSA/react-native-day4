import { createStore , combineReducers} from 'redux'
import TodosReducer from './TodosReducer'
import CompleteReducer from './CompleteReducer'

const reducer = combineReducers({
  todos: TodosReducer,
  completes: CompleteReducer
})

const store = createStore(reducer)
export default store